package org.example.connections.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "connection")
data class ConnectionEntity(var name: String, var host: String, var port: Int) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
}
