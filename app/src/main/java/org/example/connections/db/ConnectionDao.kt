package org.example.connections.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ConnectionDao {
    @Query("select * from connection order by name asc")
    fun fetchAll(): LiveData<List<ConnectionEntity>>

    @Insert
    fun insert(connection: ConnectionEntity)

    @Insert
    fun insertAll(vararg connections: ConnectionEntity)

    @Update
    fun update(connection: ConnectionEntity)

    @Delete
    fun delete(connection: ConnectionEntity)

    @Query("delete from connection")
    fun deleteAll()
}
