package org.example.connections.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlin.concurrent.thread

@Database(entities = [ConnectionEntity::class], version = 1, exportSchema = false)
abstract class ConnectionDb : RoomDatabase() {
    abstract fun connectionDao(): ConnectionDao

    companion object {
        @Volatile
        private var instance: ConnectionDb? = null

        fun fetchDb(context: Context): ConnectionDb = instance ?: synchronized(this) {
            val newInstance = Room.databaseBuilder(
                context.applicationContext,
                ConnectionDb::class.java,
                "connection_db"
            ).addCallback(object : RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    populateDb(context)
                }
            }).build()
            instance = newInstance
            newInstance
        }

        private fun populateDb(context: Context) = thread {
            val db = fetchDb(context)
            val conn1 = ConnectionEntity(name = "Garage", host = "192.168.1.1", port = 8080)
            val conn2 = ConnectionEntity(name = "Tower", host = "192.168.5.24", port = 10000)
            db.connectionDao().insertAll(conn1, conn2)
        }
    }


}
