package org.example.connections

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.example.connections.db.ConnectionEntity

class ConnectionListAdapter internal constructor(context: Context) :
    RecyclerView.Adapter<ConnectionListAdapter.ConnectionViewHolder>() {
    private val inflater = LayoutInflater.from(context)
    private val connections: MutableList<ConnectionEntity> = mutableListOf()

    fun replaceConnections(connections: List<ConnectionEntity>) {
        this.connections.clear()
        this.connections.addAll(connections)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConnectionViewHolder {
        val itemView = inflater.inflate(R.layout.item_conn, parent, false)
        return ConnectionViewHolder(itemView)
    }

    override fun getItemCount(): Int = connections.size

    override fun onBindViewHolder(holder: ConnectionViewHolder, pos: Int) {
        holder.bind(connections[pos])
    }

    fun addConnection(conn: ConnectionEntity) {
        connections += conn
        notifyItemInserted(connections.size - 1)
    }

    inner class ConnectionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val nameLbl: TextView = itemView.findViewById(R.id.nameLbl)
        private val hostLbl: TextView = itemView.findViewById(R.id.hostLbl)
        private val portLbl: TextView = itemView.findViewById(R.id.portLbl)

        fun bind(conn: ConnectionEntity) {
            nameLbl.text = conn.name
            hostLbl.text = conn.host
            portLbl.text = "${conn.port}"
        }
    }
}
