package org.example.connections

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_add_connection.*
import org.example.connections.db.ConnectionDb
import org.example.connections.db.ConnectionEntity
import kotlin.concurrent.thread

class AddConnectionActivity : AppCompatActivity() {
    private lateinit var db: ConnectionDb

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_connection)
        db = ConnectionDb.fetchDb(this)
        setupAddBtn()
    }

    private fun setupAddBtn() {
        addBtn.setOnClickListener {
            val name = "${nameTxt.text}"
            val host = "${hostTxt.text}"
            val port = "${portTxt.text}".toInt()
            createConnection(name = name, host = host, port = port)
            finish()
        }
    }

    private fun createConnection(name: String, host: String, port: Int) = thread {
        val conn = ConnectionEntity(name = name, host = host, port = port)
        db.connectionDao().insert(conn)
    }
}
