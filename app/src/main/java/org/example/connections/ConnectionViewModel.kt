package org.example.connections

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import org.example.connections.db.ConnectionDb
import org.example.connections.db.ConnectionEntity

class ConnectionViewModel(private val app: Application) : AndroidViewModel(app) {
    fun allConnections(): LiveData<List<ConnectionEntity>> = ConnectionDb.fetchDb(app).connectionDao().fetchAll()
}
